Rebin+sum ROI collection
========================

The results of an `stscan` can be processed as described in
`J. Appl. Cryst. (2021). 54, 1088-1099 <https://doi.org/10.1107/S1600576721005288>`_.

.. literalinclude :: ../../examples/rebinsum.json
   :language: json

You can run it manually as follows

.. code-block:: bash

    ewoks execute examples/rebinsum.json \
        -p wait:filename=/path/to/dataset.h5 \
        -p 'wait:entries=["1.1","2.1"]' \
        -p rebin:delta2theta=0.003 \
        -p sum:binsize=0.002 \
        -p rebin:outprefix=hc5204 \
        -p primary_outdir=/data/visitor/hc5204/id22/20230404/PROCESSED_DATA \
        --inputs=all

Workflow parameters are specified by the pattern `-p ID:NAME=VALUE` where *ID* is the id of the node
in the graph (see above) and *NAME* is the name of the parameter to be set.
