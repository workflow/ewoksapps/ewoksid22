ewoksid22 |version|
===================

*ewoksid22* provides data processing workflows for ID22.

*ewoksid22* has been developed by the `Software group <http://www.esrf.fr/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.fr/>`_.

Getting started
---------------

Install the project

.. code-block:: bash

    # pip install multianalyzer@git+https://github.com/kif/multianalyzer.git@main
    pip install multianalyzer@git+https://github.com/woutdenolf/multianalyzer.git@before_api_change
    pip install ewoksid22

The following command line scripts are installed:

* `id22sumepy`: python wrapper for `id22sume` and `id22sumalle`
* `id22sume`: fortran program
* `id22sumalle`: fortran program
* `zingit`: awk program

Execute a workflow that converts HDF5 *stscan* data to SPEC format

.. code-block:: bash

    ewoks execute examples/convert.json \
        -p convert:filename=/path/to/data.h5

The workflow definition looks like this

.. code-block:: json

    {
    "nodes": [
        {
        "task_type": "class",
        "task_identifier": "ewoksid22.convert.ID22H5ToSpec",
        "default_inputs": [
            {
                "name": "outdirs",
                "value": {
                    "primary": "pyresults/dat1",
                    "secondary": "pyresults/dat2"
                }
            },
            {
                "name": "outprefix",
                "value": "ewokstest"
            }
        ],
        "id": "convert"
        }
    ],
    }

For usage *BLISS* see the `blissoda <https://blissoda.readthedocs.io/en/latest/tutorials/id22.html>`_ project.

.. toctree::
    :hidden:

    workflows/index
    api
