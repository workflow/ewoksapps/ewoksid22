# CHANGELOG.md

## 0.2.0 (unreleased)

Drop Python 3.7

## 0.1.1

Add support for Python 3.12

## 0.1.0

Added:

- HDF5 to ASCII conversion
- Wait for scan to finish
- multianalyser data processing
- Extract data for Topas processing
