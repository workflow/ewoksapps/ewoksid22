#!/usr/bin/env bash

unset HDF5_PLUGIN_PATH

ewoks execute /data/id22/inhouse/ewoks/rebinsum.json \
    -p wait:filename=/data/id22/inhouse/id222207/id22/20220701/CCl4/CCl4_230K/CCl4_230K.h5 \
    -p rebin:parsfile=/data/id22/inhouse/andy/CCl4_0722/CCl4_processing1/out7.pars \
    -p sum:resfile=/data/id22/inhouse/andy/CCl4_0722/CCl4_processing1/temp.res \
    -p rebin:outprefix=id222207 \
    -p sum:sum_single=0 \
    -p sum:sum_all=0
